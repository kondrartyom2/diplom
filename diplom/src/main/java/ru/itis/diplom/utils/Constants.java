package ru.itis.diplom.utils;

import lombok.experimental.UtilityClass;

import java.util.Set;

public class Constants {

    public static final String SERP_API_KEY = "a2e4b1b7ef6ca50da530c834965a4039c84fa5aff50c06a76538daeee4a54270";
    public static final String GOOGLE_SEARCH_PARAMETER = "q";
    public static final String YANDEX_SEARCH_PARAMETER = "text";
    public static final String LOCATION_PARAMETER = "location";
    public static final String ENGINE_PARAMETER = "engine";
    public static final String HL_PARAMETER = "hl";
    public static final String GL_PARAMETER = "gl";
    public static final String API_KEY_PARAMETER = "api_key";
    public static final String MOSCOW_LOCATION = "Moscow,Moscow,Russia";
    public static final String GOOGLE = "google";
    public static final String YANDEX = "yandex";
    public static final String RUSSIAN_LANGUAGE = "ru";

    public static final String ORGANIC_RESULT = "organic_results";
    public static final String KNOWLEDGE_GRAPH = "knowledge_graph";
    public static final String DESCRIPTION = "description";
    public static final String POSITION = "position";
    public static final String LINK = "link";
    public static final String SNIPPET = "snippet";

    public static final Set<String> QUESTION_FRAZE = Set.of("как", "что", "сколько", "где", "какой", "кто", "когда",
            "почему", "чем", "чего", "куда", "который", "кому", "зачем", "откуда", "чей", "чья", "каков", "отчего",
            "такое", "какое");
}
