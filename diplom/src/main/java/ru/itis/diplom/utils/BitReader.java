
package ru.itis.diplom.utils;

import java.nio.ByteBuffer;
import java.nio.LongBuffer;
import java.util.BitSet;

public class BitReader {

  private final BitSet bs;
  private int pos = -1;

  public BitReader(long... n) {
    bs = BitSet.valueOf(n);
  }

  public BitReader(byte... b) {
    bs = BitSet.valueOf(b);
  }

  public BitReader(BitWriter w) {
    bs = w.toBitSet();
  }

  public BitReader(BitSet b) {
    bs = b;
  }

  public BitReader(String s) {
    bs = BitSet.valueOf(s.getBytes());
  }

  public BitReader(ByteBuffer b) {
    bs = BitSet.valueOf(b);
  }

  public BitReader(LongBuffer b) {
    bs = BitSet.valueOf(b);
  }

  public BitReader(BitReader other) {
    bs = other.bs;
    pos = other.pos;
  }

  public boolean readBit() {
    return bs.get(++pos);
  }

  public BitWriter readBits(int size) {
    BitWriter w = new BitWriter(size);
    for (int i = 0; (i < size) && hasUnreadBits(); ++i) {
      w.writeBit(readBit());
    }
    return w;
  }

  public String readString(int size) {
    return readBits(size).toString();
  }

  public BitSet readBitSet(int size) {
    return readBits(size).toBitSet();
  }

  public long[] readLongArray(int size) {
    return readBits(size).toLongArray();
  }

  public byte[] readByteArray(int size) {
    return readBits(size).toByteArray();
  }

  public long readLong() {
    return readBits(64).toLong();
  }

  public int readInt() {
    return readBits(32).toInt();
  }

  public short readShort() {
    return readBits(16).toShort();
  }

  public char readChar() {
    return readBits(16).toChar();
  }

  public byte readByte() {
    return readBits(8).toByte();
  }

  public int getReadiedSize() {
    return pos + 1;
  }

  public int size() {
    return bs.size();
  }

  public int getUnreadSize() {
    return bs.size() - pos - 1;
  }

  public boolean hasUnreadBits() {
    return getUnreadSize() > 0;
  }
}