package ru.itis.diplom.utils;

import ru.itis.diplom.dto.MorphologyTag;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Objects.hash;
import static ru.itis.diplom.utils.AotReader.readLemmas;
import static ru.itis.diplom.utils.AotReader.readMorph;
import static ru.itis.diplom.utils.AotReader.readRefs;
import static ru.itis.diplom.utils.AotReader.readStrings;
import static ru.itis.diplom.utils.ByteBlock.readBlockFrom;
import static ru.itis.diplom.utils.PartOfSpeech.partOfSpeech;

public class WordFormMeaning {

  private static final MorphologyTag[][] allMorphologyTags;
  private static final String[] allFlexionStrings;
  private static final int[][] lemmas;
  private static final Map<Integer, int[]> refs;


  private final int lemmaId;


  private final int transformationIndex;

  static {
    try (DataInputStream file =
           new DataInputStream(new GZIPInputStream(WordFormMeaning.class.getResourceAsStream("/mrd.gz")))
    ) {
      allMorphologyTags = readMorph(readBlockFrom(file));
      allFlexionStrings = readStrings(readBlockFrom(file));
      lemmas = readLemmas(readBlockFrom(file));
      refs = readRefs(readBlockFrom(file));
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }


  public static List<String> listAllFlexions() {
    return asList(allFlexionStrings);
  }

  private WordFormMeaning(int lemmaId, int transformationIndex) {
    this.lemmaId = lemmaId;
    this.transformationIndex = transformationIndex;
  }

  private static int getTransformationsSize(int lemmaId) {
    return lemmas[lemmaId].length / 2;
  }

  private static String getTransformationString(int lemmaId, int transformationIndex) {
    return allFlexionStrings[lemmas[lemmaId][transformationIndex * 2]];
  }

  private static List<MorphologyTag> getTransformationMorphology(int lemmaId, int transformationIndex) {
    return asList(allMorphologyTags[lemmas[lemmaId][transformationIndex * 2 + 1]]);
  }

  public static List<WordFormMeaning> lookupForMeanings(String w) {
    w = w.toLowerCase().replace('ё', 'е');
    int[] ids = refs.get(w.hashCode());
    if (ids == null) {
      return emptyList();
    }
    List<WordFormMeaning> meanings = new ArrayList<>();
    for (int lemmaId : ids) {
      for (int flexionIdx = 0; flexionIdx < getTransformationsSize(lemmaId); ++flexionIdx) {
        if (getTransformationString(lemmaId, flexionIdx).equals(w)) {
          meanings.add(new WordFormMeaning(lemmaId, flexionIdx));
        }
      }
    }
    return meanings;
  }

  public static WordFormMeaning lookupForMeaning(long id) throws IOException {
    BitReader reader = new BitReader(id);
    int lemmaId = reader.readInt();
    int flexionIndex = reader.readInt();
    return new WordFormMeaning(lemmaId, flexionIndex);
  }

  public long getId() {
    return new BitWriter()
      .writeInt(lemmaId)
      .writeInt(transformationIndex)
      .toLong();
  }


  @Override
  public String toString() {
    return getTransformationString(lemmaId, transformationIndex);
  }

  public WordFormMeaning getLemma() {
    return new WordFormMeaning(lemmaId, 0);
  }


  public List<MorphologyTag> getMorphology() {
    return getTransformationMorphology(lemmaId, transformationIndex);
  }


  public List<WordFormMeaning> getTransformations() {
    WordFormMeaning[] res = new WordFormMeaning[getTransformationsSize(lemmaId)];
    for (int i = 0; i < res.length; ++i) {
      res[i] = new WordFormMeaning(lemmaId, i);
    }
    return asList(res);
  }


  public PartOfSpeech getPartOfSpeech() {
    return partOfSpeech(getMorphology());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WordFormMeaning that = (WordFormMeaning) o;
    return lemmaId == that.lemmaId && transformationIndex == that.transformationIndex;
  }

  @Override
  public int hashCode() {
    return hash(lemmaId, transformationIndex);
  }
}
