
package ru.itis.diplom.utils;

import java.io.ByteArrayOutputStream;
import java.util.BitSet;

public class BitWriter {

  private final BitSet bs;
  private int pos = -1;

  public BitWriter() {
    this(64);
  }

  public BitWriter(int size) {
    bs = new BitSet(size);
  }

  public BitWriter(BitWriter other) {
    bs = (BitSet) other.bs.clone();
    pos = other.pos;
  }

  public long toLong() {
    long[] arr = bs.toLongArray();
    return arr.length == 0 ? 0 : arr[0];
  }

  public byte[] toByteArray() {
    return bs.toByteArray();
  }

  public BitSet toBitSet() {
    return (BitSet) bs.clone();
  }

  public BitReader toBitReader() {
    return new BitReader(this);
  }

  public long[] toLongArray() {
    return bs.toLongArray();
  }

  public int toInt() {
    return (int) toLong();
  }

  public short toShort() {
    return (short) toLong();
  }

  public char toChar() {
    return (char) toLong();
  }

  @Override
  public String toString() {
    BitReader bytesReader = new BitReader(this);
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    while (bytesReader.hasUnreadBits()) {
      outputStream.write(bytesReader.readByte());
    }
    return outputStream.toString();
  }

  public byte toByte() {
    byte[] arr = bs.toByteArray();
    return arr.length == 0 ? 0 : arr[0];
  }

  public BitWriter writeBit(boolean bit) {
    bs.set(++pos, bit);
    return this;
  }

  public BitWriter writeBits(BitReader r, int len) {
    for (int i = 0; i < len; ++i) {
      writeBit(r.readBit());
    }
    return this;
  }

  public BitWriter writeLong(long l) {
    return writeBits(new BitReader(l), 64);
  }

  public BitWriter writeInt(int i) {
    return writeBits(new BitReader(i), 32);
  }

  public BitWriter writeShort(short s) {
    return writeBits(new BitReader(s), 16);
  }

  public BitWriter writeChar(char s) {
    return writeBits(new BitReader(s), 16);
  }

  public BitWriter writeString(String s) {
    for (byte c : s.getBytes()) {
      writeByte(c);
    }
    return this;
  }

  public BitWriter writeByte(byte b) {
    return writeBits(new BitReader(b), 8);
  }

  public int size() {
    return bs.size();
  }

  public int getUsedSize() {
    return pos + 1;
  }

  public int getFreeSize() {
    return bs.size() - pos - 1;
  }

  public boolean hasFreeBits() {
    return getFreeSize() > 0;
  }
}