package ru.itis.diplom.utils;

import org.apache.lucene.analysis.ASCIIFoldingFilter;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.PorterStemFilter;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.ClassicFilter;
import org.apache.lucene.analysis.standard.ClassicTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;
import ru.itis.diplom.dto.Keyword;

import java.io.IOException;
import java.io.StringReader;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class Analyzer {

    public static Boolean isCorrectQuestion(String request) {
        Pattern russianLanguage = Pattern.compile("[а-яА-ЯЁё\s?]*");
        return russianLanguage.matcher(request).matches() &&
                !(request.contains("почему") || request.contains("как") || request.contains("зачем"));
    }

    public static String stem(String term) throws IOException {

        TokenStream tokenStream = null;
        try {

            tokenStream = new ClassicTokenizer(Version.LUCENE_36, new StringReader(term));
            tokenStream = new PorterStemFilter(tokenStream);

            Set<String> stems = new HashSet<String>();
            CharTermAttribute token = tokenStream.getAttribute(CharTermAttribute.class);
            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                stems.add(token.toString());
            }

            if (stems.size() != 1) {
                return null;
            }
            String stem = stems.iterator().next();
            if (!stem.matches("[а-яА-ЯЁё-]+")) {
                return null;
            }

            return stem;

        } finally {
            if (tokenStream != null) {
                tokenStream.close();
            }
        }

    }

    public static Set<Keyword> guessFromString(String input) throws IOException {

        TokenStream tokenStream = null;
        try {

            input = input.replaceAll("-+", "-0");
            input = input.replaceAll("[\\p{Punct}&&[^'-]]+", " ");
            input = input.replaceAll("(?:'(?:[tdsm]|[vr]e|ll))+\\b", "");

            tokenStream = new ClassicTokenizer(Version.LUCENE_36, new StringReader(input));
            tokenStream = new LowerCaseFilter(Version.LUCENE_36, tokenStream);
            tokenStream = new ClassicFilter(tokenStream);
            tokenStream = new ASCIIFoldingFilter(tokenStream);
            tokenStream = new StopFilter(Version.LUCENE_36, tokenStream, Collections.emptySet());

            List<Keyword> keywords = new LinkedList<Keyword>();
            CharTermAttribute token = tokenStream.getAttribute(CharTermAttribute.class);
            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                String term = token.toString();
                String stem = stem(term);
                if (stem != null) {
                    Keyword keyword = find(keywords, new Keyword(stem.replaceAll("-0", "-")));
                    keyword.add(term.replaceAll("-0", "-"));
                }
            }
            Collections.sort(keywords);

            return new HashSet<>(keywords);
        } finally {
            if (tokenStream != null) {
                tokenStream.close();
            }
        }
    }

    public static <T> T find(Collection<T> collection, T example) {
        for (T element : collection) {
            if (element.equals(example)) {
                return element;
            }
        }
        collection.add(example);
        return example;
    }
}
