package ru.itis.diplom.utils;

import java.io.DataInputStream;
import java.io.IOException;


class ByteBlock {

  private final int linesCount;
  private final byte[] bytes;

  private ByteBlock(int linesCount, byte[] bytes) {
    this.linesCount = linesCount;
    this.bytes = bytes;
  }

  int getLinesCount() {
    return linesCount;
  }

  byte[] getBytes() {
    return bytes;
  }

  static ByteBlock readBlockFrom(DataInputStream file) throws IOException {
    int size = file.readInt();
    byte[] bytes = new byte[file.readInt()];
    file.readFully(bytes);
    return new ByteBlock(size, bytes);
  }
}
