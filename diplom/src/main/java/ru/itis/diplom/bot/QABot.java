
package ru.itis.diplom.bot;

import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.itis.diplom.config.TelegramProperties;
import ru.itis.diplom.repository.AnswerRepository;
import ru.itis.diplom.service.QAService;
import ru.itis.diplom.utils.Analyzer;

@Slf4j
@Service
@RequiredArgsConstructor
public class QABot extends TelegramLongPollingBot {
    private final TelegramProperties properties;
    private final AnswerRepository repository;
    private final QAService service;

    public String getBotUsername() { return properties.getName(); }
    @Override
    public String getBotToken() { return properties.getToken(); }

    @SneakyThrows
    @Override
    public void onUpdateReceived(@NotNull Update update) {
        SendMessage message = new SendMessage();
        message.setChatId(update.getMessage().getChatId());
        String request = update.getMessage().getText().trim().toLowerCase();
        if (!Analyzer.isCorrectQuestion(request)) {
            message.setText("Не корректный вопрос!");
            execute(message);
            return;
        }
        message.setText(service.run(request).toString());
        message.setParseMode("html");
        execute(message);
    }
}