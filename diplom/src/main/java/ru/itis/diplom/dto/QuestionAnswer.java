package ru.itis.diplom.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QuestionAnswer {

    private String title;

    private List<SearchAnswer> searchAnswers;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getTitle());
        for(int i = 0; i < searchAnswers.size(); i++) {
            builder.append("\n").append(i + 1).append(") ")
                    .append("<b><a href=\"").append(searchAnswers.get(i).getLink()).
                    append("\">Ссылка</a></b>");
        }
        return builder.toString();
    }
}
