package ru.itis.diplom.service;

import org.springframework.data.util.Pair;
import ru.itis.diplom.dto.QuestionAnswer;
import ru.itis.diplom.dto.SearchAnswer;

import java.util.List;

public interface AnswerService {
    QuestionAnswer getAnswer(Pair<Pair<String, List<SearchAnswer>>, Pair<String, List<SearchAnswer>>> pairs);
}
