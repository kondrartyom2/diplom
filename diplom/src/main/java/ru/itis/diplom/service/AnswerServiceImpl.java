package ru.itis.diplom.service;

import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import ru.itis.diplom.dto.QuestionAnswer;
import ru.itis.diplom.dto.SearchAnswer;
import ru.itis.diplom.utils.AnswerUtils;

import java.util.List;

@Service
public class AnswerServiceImpl implements AnswerService {

    @Override
    public QuestionAnswer getAnswer(Pair<Pair<String, List<SearchAnswer>>, Pair<String, List<SearchAnswer>>> pairs) {
        return AnswerUtils.getAnswer(AnswerUtils.rankAnswers(pairs));
    }
}
