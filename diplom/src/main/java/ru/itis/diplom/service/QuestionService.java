package ru.itis.diplom.service;

import org.springframework.data.util.Pair;
import ru.itis.diplom.dto.SearchAnswer;

import java.io.IOException;
import java.util.List;

public interface QuestionService {
    Pair<Pair<String, List<SearchAnswer>>, Pair<String, List<SearchAnswer>>> extractAnswerCandidates(
            String request) throws IOException;
}
