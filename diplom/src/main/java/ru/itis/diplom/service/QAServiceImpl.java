package ru.itis.diplom.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.diplom.dto.QuestionAnswer;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class QAServiceImpl implements QAService {
    private final AnswerService answerService;
    private final QuestionService questionService;

    @Override
    public QuestionAnswer run(String request) {
        String question = request.trim().toLowerCase();
        try {
            return answerService.getAnswer(questionService.extractAnswerCandidates(question));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
