package ru.itis.diplom.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import ru.itis.diplom.dto.SearchAnswer;
import serpapi.GoogleSearch;
import serpapi.SerpApiSearchException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static ru.itis.diplom.utils.Constants.API_KEY_PARAMETER;
import static ru.itis.diplom.utils.Constants.DESCRIPTION;
import static ru.itis.diplom.utils.Constants.ENGINE_PARAMETER;
import static ru.itis.diplom.utils.Constants.GL_PARAMETER;
import static ru.itis.diplom.utils.Constants.GOOGLE;
import static ru.itis.diplom.utils.Constants.GOOGLE_SEARCH_PARAMETER;
import static ru.itis.diplom.utils.Constants.HL_PARAMETER;
import static ru.itis.diplom.utils.Constants.KNOWLEDGE_GRAPH;
import static ru.itis.diplom.utils.Constants.LINK;
import static ru.itis.diplom.utils.Constants.LOCATION_PARAMETER;
import static ru.itis.diplom.utils.Constants.MOSCOW_LOCATION;
import static ru.itis.diplom.utils.Constants.ORGANIC_RESULT;
import static ru.itis.diplom.utils.Constants.POSITION;
import static ru.itis.diplom.utils.Constants.RUSSIAN_LANGUAGE;
import static ru.itis.diplom.utils.Constants.SERP_API_KEY;
import static ru.itis.diplom.utils.Constants.SNIPPET;

@Service("googleSearcher")
public class GoogleSearcher implements Searcher {

    @Override
    public Pair<String, List<SearchAnswer>> getSearchResult(String request) {
        Map<String, String> parameter = new HashMap<>();
        parameter.put(GOOGLE_SEARCH_PARAMETER, request);
        parameter.put(ENGINE_PARAMETER, GOOGLE);
        parameter.put(LOCATION_PARAMETER, MOSCOW_LOCATION);
        parameter.put(HL_PARAMETER, RUSSIAN_LANGUAGE);
        parameter.put(GL_PARAMETER, RUSSIAN_LANGUAGE);
        parameter.put(API_KEY_PARAMETER, SERP_API_KEY);

        GoogleSearch search = new GoogleSearch(parameter);
        List<SearchAnswer> resultSearch = new ArrayList<>();

        try {
            JsonArray results = (JsonArray) search.getJson().get(ORGANIC_RESULT);
            for (JsonElement element : results) {
                JsonObject asJsonObject = element.getAsJsonObject();
                resultSearch.add(SearchAnswer.builder()
                        .link(asJsonObject.get(LINK).getAsString())
                        .position(asJsonObject.get(POSITION).getAsInt())
                        .snippet(asJsonObject.get(SNIPPET).getAsString())
                        .build()
                );
            }
            String description = Objects.nonNull(
                    Optional.ofNullable(search.getJson().get(KNOWLEDGE_GRAPH).getAsJsonObject().get(DESCRIPTION))
                            .map(JsonElement::getAsString).orElse("")
            ) ? search.getJson().get(KNOWLEDGE_GRAPH).getAsJsonObject().get(DESCRIPTION).getAsString() :
                    "";
            return Pair.of(description,
                    resultSearch.subList(0, 5));
        } catch (SerpApiSearchException e) {
            System.out.println("Exception");
            return null;
        }
    }
}