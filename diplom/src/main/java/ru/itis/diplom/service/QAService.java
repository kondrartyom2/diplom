package ru.itis.diplom.service;

import ru.itis.diplom.dto.QuestionAnswer;

public interface QAService {
    QuestionAnswer run(String request);
}
