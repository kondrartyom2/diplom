package ru.itis.diplom.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import ru.itis.diplom.dto.SearchAnswer;
import serpapi.SerpApiSearchException;
import serpapi.YandexSearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static ru.itis.diplom.utils.Constants.DESCRIPTION;
import static ru.itis.diplom.utils.Constants.KNOWLEDGE_GRAPH;
import static ru.itis.diplom.utils.Constants.LINK;
import static ru.itis.diplom.utils.Constants.ORGANIC_RESULT;
import static ru.itis.diplom.utils.Constants.POSITION;
import static ru.itis.diplom.utils.Constants.SERP_API_KEY;
import static ru.itis.diplom.utils.Constants.SNIPPET;
import static ru.itis.diplom.utils.Constants.YANDEX_SEARCH_PARAMETER;

@Service("yandexSearcher")
public class YandexSearcher implements Searcher {

    @Override
    public Pair<String, List<SearchAnswer>> getSearchResult(String request) {
        Map<String, String> parameter = new HashMap<>();
        parameter.put(YANDEX_SEARCH_PARAMETER, request);

        YandexSearch search = new YandexSearch(parameter, SERP_API_KEY);
        List<SearchAnswer> resultSearch = new ArrayList<>();

        try {
            JsonArray results = (JsonArray) search.getJson().get(ORGANIC_RESULT);
            for (JsonElement element : results) {
                JsonObject asJsonObject = element.getAsJsonObject();
                resultSearch.add(SearchAnswer.builder()
                        .link(asJsonObject.get(LINK).getAsString())
                        .position(asJsonObject.get(POSITION).getAsInt())
                        .snippet(asJsonObject.get(SNIPPET).getAsString())
                        .build()
                );
            }
            String description = Objects.nonNull(
                    Optional.ofNullable(search.getJson().get(KNOWLEDGE_GRAPH).getAsJsonObject().get(DESCRIPTION))
                            .map(JsonElement::getAsString).orElse("")
            ) ? search.getJson().get(KNOWLEDGE_GRAPH).getAsJsonObject().get(DESCRIPTION).getAsString() :
                    "";
            return Pair.of(description,
                    resultSearch.subList(0, 5)
            );
        } catch (SerpApiSearchException e) {
            System.out.println("Exception");
            return null;
        }
    }
}