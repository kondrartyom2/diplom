package ru.itis.diplom.service;

import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import ru.itis.diplom.dto.Keyword;
import ru.itis.diplom.dto.SearchAnswer;
import ru.itis.diplom.utils.Analyzer;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static ru.itis.diplom.utils.Constants.QUESTION_FRAZE;
import static ru.itis.diplom.utils.WordFormMeaning.lookupForMeanings;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final Searcher googleSearcher;
    private final Searcher yandexSearcher;

    public QuestionServiceImpl() {
        this.googleSearcher = new YandexSearcher();
        this.yandexSearcher = new GoogleSearcher();
    }

    @Override
    public Pair<Pair<String, List<SearchAnswer>>, Pair<String, List<SearchAnswer>>> extractAnswerCandidates(
            String request) throws IOException {
        Set<String> keywords = Analyzer.guessFromString(request).stream()
                .map(Keyword::getStem)
                .filter(elem -> !QUESTION_FRAZE.contains(elem))
                .collect(Collectors.toSet());
        request = request.replaceAll("[?]", "");
        String question = "";
        for (String word : request.split(" ")) {
            if (keywords.contains(word)) {
                question += " ".concat(word);
            }
        }
        String resultQuestion = "";
        for (String word : question.split(" ")) {
            var meaning = lookupForMeanings(word);
            if (meaning.size() < 1) {
                resultQuestion += word;
            } else {
                resultQuestion += " ".concat(meaning.get(0).getLemma().toString());
            }
        }
        Pair<String, List<SearchAnswer>> googleSearcherSearchResult = googleSearcher.getSearchResult(request);
        Pair<String, List<SearchAnswer>> yandexSearcherSearchResult = yandexSearcher.getSearchResult(request);
        return Pair.of(googleSearcherSearchResult, yandexSearcherSearchResult);
    }
}
