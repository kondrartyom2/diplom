package ru.itis.diplom.service;

import org.springframework.data.util.Pair;
import ru.itis.diplom.dto.SearchAnswer;

import java.io.IOException;
import java.util.List;

public interface Searcher {

    Pair<String, List<SearchAnswer>> getSearchResult(String request);
}
