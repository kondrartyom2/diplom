package ru.itis.diplom.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@ConfigurationProperties(prefix = "telegram.bot")
public class TelegramProperties {

    private String name;
    private String token;
    private String chatId;
}
