package ru.itis.diplom.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.diplom.dto.QuestionAnswer;
import ru.itis.diplom.service.QAService;

@RestController
@RequiredArgsConstructor
public class SearchController {

    private final QAService service;

    @GetMapping("/")
    public QuestionAnswer getAnswer(@RequestParam("question") String request) {
        return service.run(request);
    }
}
